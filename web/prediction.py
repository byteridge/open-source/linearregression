import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import statsmodels.api as sm
import pickle

df = pd.read_csv("Salary_Data.csv")

x = df['YearsExperience']
y = df['Salary']

regressor = sm.OLS(y,x).fit()

pickle.dump(regressor, open('model.pkl','wb'))

model = pickle.load(open('model.pkl','rb'))
print(model.predict(3.0))